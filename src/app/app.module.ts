import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { MyApp } from './app.component';

import { RemoteIoPage } from '../pages/remote-io/remote-io';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { TemperaturePage } from '../pages/temperature/temperature';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push } from '@ionic-native/push';

@NgModule({
  declarations: [
    MyApp,
    RemoteIoPage,
    DashboardPage,
    TemperaturePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DashboardPage,
    RemoteIoPage,
    TemperaturePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Push,
    SpeechRecognition
  ]
})
export class AppModule {}
