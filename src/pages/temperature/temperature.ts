import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Paho } from 'ng2-mqtt/mqttws31';
import $ from "jquery";
import * as HighCharts from 'highcharts';
import HighchartsMore from 'highcharts-more';
HighchartsMore(HighCharts);

@IonicPage()
@Component({
  selector: 'page-temperature',
  templateUrl: 'temperature.html',
})
export class TemperaturePage {
  private _client: Paho.MQTT.Client;
  isConnected = false;
  hasPermission = false;
  GPIO: number;
  Status: string;
  chart;
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {

    const clientID = 'web_' + Math.random() * 100;
    this._client = new Paho.MQTT.Client("m23.cloudmqtt.com", 31472, clientID);

    this._client.onConnectionLost = (responseObject: Object) => {
      let toast = this.toastCtrl.create({
        message: 'Connection lost...',
        duration: 2000,
        position: 'top'
      });

      toast.present(toast);
      this.isConnected = false;
      this._client.unsubscribe('StatusRPI',null);
    };

    this._client.onMessageArrived = (message: Paho.MQTT.Message) => {
      if(message.destinationName == "StatusRPI"){
        this.showMessageBroker(message.payloadString);
      }
      if(message.destinationName == "InfoRPI"){
        var data = message.payloadString.split(";", 2);
        var temperature = data[1].split(":",2)[1];
        this.ChangeGauge(temperature);
      }
    };

    this._client.connect({
      useSSL: true,
      userName: "zgbotdfy",
      password: "7GPCBT0VUAdf",
      onSuccess: this.onConnected.bind(this),
      onFailure: this.doFail.bind(this)
    });
  }

  private onConnected():void {
    let toast = this.toastCtrl.create({
      message: 'Connected with broker',
      duration: 2000,
      position: 'top'
    });

    toast.present(toast);
    this._client.subscribe('StatusRPI',null);
    this._client.subscribe('InfoRPI',null);
    this.isConnected = true;
  }

  doFail() {
    let toast = this.toastCtrl.create({
      message: 'Failed to connect with broker...',
      duration: 2000,
      position: 'top'
    });

    toast.present(toast);
  }

  showMessageBroker(message){
    let toast = this.toastCtrl.create({
      message: 'GPIO status changed: ' + message,
      duration: 2000,
      position: 'bottom'
    });

    toast.present(toast);
  }

  ChangeGauge(temperature){
    var point = this.chart.series[0].points[0];
    point.update(parseInt(temperature));
  }

  ionViewDidLoad() {
    this.chart = HighCharts.chart('container', {
      chart: {
          type: 'gauge',
          plotBackgroundColor: null,
          plotBackgroundImage: null,
          plotBorderWidth: 0,
          plotShadow: false
      },

      title: {
          text: ''
      },

      pane: {
          startAngle: -150,
          endAngle: 150,
          background: [{
              backgroundColor: {
                  linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                  stops: [
                      [0, '#FFF'],
                      [1, '#333']
                  ]
              },
              borderWidth: 0,
              outerRadius: '109%'
          }, {
              backgroundColor: {
                  linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                  stops: [
                      [0, '#333'],
                      [1, '#FFF']
                  ]
              },
              borderWidth: 1,
              outerRadius: '107%'
          }, {
              // default background
          }, {
              backgroundColor: '#DDD',
              borderWidth: 0,
              outerRadius: '105%',
              innerRadius: '103%'
          }]
      },

      // the value axis
      yAxis: {
          min: -10,
          max: 40,
          minorTickInterval: 'auto',
          minorTickWidth: 1,
          minorTickLength: 10,
          minorTickPosition: 'inside',
          minorTickColor: '#666',
          tickPixelInterval: 30,
          tickWidth: 2,
          tickPosition: 'inside',
          tickLength: 10,
          tickColor: '#666',
          labels: {
              step: 1,
              rotation: 'auto'
          },
          title: {
              text: '°C'
          },
          plotBands: [{
            from: -10,
            to: 0,
            color: '#42aaf4' // blue
          },{
              from: 18,
              to: 25,
              color: '#55BF3B' // green
          }, {
              from: 25,
              to: 30,
              color: '#DDDF0D' // yellow
          }, {
              from: 30,
              to: 40,
              color: '#DF5353' // red
          }]
      },
      series: [{
          name: 'Temperature',
          data: [0],
          tooltip: {
              valueSuffix: ' °C'
          }
      }]
    });
  }
}
