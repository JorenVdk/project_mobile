import { Component, OnInit } from '@angular/core';
import { IonicPage, ToastController,NavController, NavParams } from 'ionic-angular';
import {SpeechRecognition, SpeechRecognitionListeningOptionsAndroid} from '@ionic-native/speech-recognition';
import {  } from 'ionic-angular';
import {Paho} from 'ng2-mqtt/mqttws31';
import $ from "jquery";

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  private _client: Paho.MQTT.Client;
  isConnected = false;
  hasPermission = false;
  GPIO: number;
  Status: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {

    const clientID = 'web_' + Math.random() * 100;
    this._client = new Paho.MQTT.Client("m23.cloudmqtt.com", 31472, clientID);

    this._client.onConnectionLost = (responseObject: Object) => {
      let toast = this.toastCtrl.create({
        message: 'Connection lost...',
        duration: 2000,
        position: 'top'
      });

      toast.present(toast);
      this.isConnected = false;
      this._client.unsubscribe('StatusRPI',null);
    };

    this._client.onMessageArrived = (message: Paho.MQTT.Message) => {
      if(message.destinationName == "StatusRPI"){
        this.showMessageBroker(message.payloadString);
      }
      if(message.destinationName == "InfoRPI"){
        var data = message.payloadString.split(";", 2);
        this.ChangeSVG(data[0]);
        var temperature = data[1].split(":",2)[1]
      }
    };

    this._client.connect({
      useSSL: true,
      userName: "zgbotdfy",
      password: "7GPCBT0VUAdf",
      onSuccess: this.onConnected.bind(this),
      onFailure: this.doFail.bind(this)
    });
  }

  private onConnected():void {
    let toast = this.toastCtrl.create({
      message: 'Connected with broker',
      duration: 2000,
      position: 'top'
    });

    toast.present(toast);
    this._client.subscribe('StatusRPI',null);
    this._client.subscribe('InfoRPI',null);
    this.isConnected = true;
  }

  doFail() {
    let toast = this.toastCtrl.create({
      message: 'Failed to connect with broker...',
      duration: 2000,
      position: 'top'
    });

    toast.present(toast);
  }

  showMessageBroker(message){
    let toast = this.toastCtrl.create({
      message: 'GPIO status changed: ' + message,
      duration: 2000,
      position: 'bottom'
    });

    toast.present(toast);
  }

  ChangeSVG(Message){
    let GPIOnumber = Message.split(" ")[0];
    let status = Message.split(" ")[1];
    let ID = "#GPIO" + GPIOnumber;
    if(status == 'On'){
      $(ID).css("fill","#00ffff");
    }
    else{
      $(ID).css("fill","#ffffff");
    }
  }
}
