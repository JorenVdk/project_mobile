import { Component, OnInit } from '@angular/core';
import { IonicPage, ToastController,NavController, NavParams } from 'ionic-angular';
import {SpeechRecognition, SpeechRecognitionListeningOptionsAndroid} from '@ionic-native/speech-recognition';
import {  } from 'ionic-angular';
import {Paho} from 'ng2-mqtt/mqttws31';
import $ from "jquery";

@IonicPage()
@Component({
  selector: 'page-remote-io',
  templateUrl: 'remote-io.html',
})

export class RemoteIoPage implements OnInit {

  canvasWidth: number
  needleValue: number
  centralLabel: string
  options
  name = 'Temperature'

  private _client: Paho.MQTT.Client;
  isConnected = false;
  hasPermission = false;
  GPIO: number;
  Status: string;
  speechOptions: SpeechRecognitionListeningOptionsAndroid;

  ngOnInit() {
    this.canvasWidth = 300
    this.needleValue = 50
    this.centralLabel = this.needleValue + ' °C'
    this.options = {
      hasNeedle: true,
      needleColor: 'gray',
      needleUpdateSpeed: 1000,
      arcColors: ["rgb(66, 215, 244)","rgb(61,204,91)","rgb(255,84,84)"],
      arcDelimiters:  [30,70],
      rangeLabel: ['-20', '40'],
      needleStartValue: -20,
      outerNeedle: false,
    }
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, private speech: SpeechRecognition) {

    const clientID = 'web_' + Math.random() * 100;
    this._client = new Paho.MQTT.Client("m23.cloudmqtt.com", 31472, clientID);

    this._client.onConnectionLost = (responseObject: Object) => {
      let toast = this.toastCtrl.create({
        message: 'Connection lost...',
        duration: 2000,
        position: 'top'
      });

      toast.present(toast);
      this.isConnected = false;
      this._client.unsubscribe('StatusRPI',null);
    };

    this._client.onMessageArrived = (message: Paho.MQTT.Message) => {
      console.log( message.destinationName + ": " + message.payloadString);
      if(message.destinationName == "StatusRPI"){
        this.showMessageBroker(message.payloadString);
      }
    };

    this._client.connect({
      useSSL: true,
      userName: "zgbotdfy",
      password: "7GPCBT0VUAdf",
      onSuccess: this.onConnected.bind(this),
      onFailure: this.doFail.bind(this)
    });

    this.speech.hasPermission()
    .then((permission: boolean) => this.hasPermission = permission)

    if(this.hasPermission == false){
      this.getPermission();
    }

  }

  private onConnected():void {
    let toast = this.toastCtrl.create({
      message: 'Connected with broker',
      duration: 2000,
      position: 'top'
    });

    toast.present(toast);
    this._client.subscribe('StatusRPI',null);
    this._client.subscribe('GPIOchanged',null);
    this.isConnected = true;
  }

  SendInfo() {
    if(this.isConnected == false){
      let toast = this.toastCtrl.create({
        message: 'Not connected with broker!',
        duration: 2000,
        position: 'top'
      });

      toast.present(toast);
    }
    else{
      let toast = this.toastCtrl.create({
        message: 'GPIO selected: '+this.GPIO +'\nStatus: ' + this.Status,
        duration: 2000,
        position: 'top'
      });

      toast.present(toast);

      this.SendMessage(this.GPIO,this.Status);
    }
  }

  doFail() {
    let toast = this.toastCtrl.create({
      message: 'Failed to connect with broker...',
      duration: 2000,
      position: 'top'
    });

    toast.present(toast);
  }

  SendMessage(topic, messageText){
    const message = new Paho.MQTT.Message(messageText);
    message.destinationName = "GPIO_" + topic;
    this._client.send(message);
  }

  showMessageBroker(message){
    let toast = this.toastCtrl.create({
      message: 'GPIO status changed: ' + message,
      duration: 2000,
      position: 'bottom'
    });

    toast.present(toast);
  }

  ReconnectWithBroker(){
    if(this.isConnected == false){
      this._client.connect({
        useSSL: true,
        userName: "zgbotdfy",
        password: "7GPCBT0VUAdf",
        onSuccess: this.onConnected.bind(this),
        onFailure: this.doFail.bind(this)
      });
    }
  }

  isSpeechSupported(){
    this.speech.isRecognitionAvailable().then((available: boolean) => {
      let toast = this.toastCtrl.create({
        message: available.toString(),
        duration: 2000,
        position: 'top'
      });

      toast.present(toast);
    });
  }

  getPermission(){
    try{
      let permission;
      this.speech.requestPermission()
      .then(
        () => permission = "granted",
        () => permission = "denied",
      );

      let toast = this.toastCtrl.create({
        message: permission,
        duration: 2000,
        position: 'top'
      });

      toast.present(toast);
    }
    catch(e){
      let toast = this.toastCtrl.create({
        message: e,
        duration: 2000,
        position: 'top'
      });

      toast.present(toast);
    }
  }

  ListenForSpeech():void{

    this.speechOptions={
      language: 'en-Us',
      matches: 1,
      prompt: "",
      showPopup: true,
      showPartial: false
    }

    this.speech.startListening(this.speechOptions).subscribe(
      data => {
        let stringArray = data[0].split(' ');
        let GPIO = stringArray[0];
        let status;
        if(stringArray[1] == "on"){
          status = "On";
        }
        else{
          status = "Off";
        }

        let toast = this.toastCtrl.create({
          message: 'GPIO selected: '+ GPIO +'\nStatus: ' + status,
          duration: 2000,
          position: 'top'
        });

        toast.present(toast);

        this.SendMessage(GPIO,status);
      },
      error => console.log(error));
  }
}
