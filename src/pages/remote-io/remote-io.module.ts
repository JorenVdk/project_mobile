import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RemoteIoPage } from './remote-io';

@NgModule({
  declarations: [
    RemoteIoPage
  ],
  imports: [
    IonicPageModule.forChild(RemoteIoPage),
  ]
})
export class RemoteIoPageModule {}
